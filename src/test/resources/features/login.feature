Feature: Close order

  Scenario Outline: Consumer tries do login
    Given the consumer "<consumer>" authenticates in sauce demo
    Then the consumer should see the error message "<error_message>"

    Examples:
        | consumer         | error_message |
        | locked_out_user  | Epic sadface: Sorry, this user has been locked out. |
        | invalid_consumer | Epic sadface: Username and password do not match any user in this service|
