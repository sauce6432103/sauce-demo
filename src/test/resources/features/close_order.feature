Feature: Close order

  @smoke
  Scenario Outline: Authenticated Consumer close an order
    Given the consumer "standardUser" authenticates in sauce demo
    When the consumer places an order with <quantity> items
    Then the order should be successfully closed

    Examples:
      | quantity |
      | 1        |
      | 2        |

  @smoke
  Scenario: Authenticated Consumer remove an item from cart
    Given the consumer "standardUser" authenticates in sauce demo
    When the consumer adds an item to cart
    And the consumer removes the item from cart
    Then the cart should be empty

  Scenario: Authenticated Consumer continue to checkout without items
    Given the consumer "standardUser" authenticates in sauce demo
    When the consumer places an order with 0 items
    Then the order should not be successfully closed
