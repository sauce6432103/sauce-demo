package steps;

import com.google.inject.Inject;
import commons.Consumer;
import commons.ScenarioContext;
import commons.Utils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import pages.LoginPage;
import java.io.IOException;

import static commons.Context.CONSUMER;

public class LoginSteps {

    @Inject
    private LoginPage loginPage;
    @Inject
    private ScenarioContext scenarioContext;
    @Inject
    private Utils utils;

    @Given("the consumer {string} authenticates in sauce demo")
    public void theConsumerAuthenticatesInSauceDemo(String consumer) throws IOException, ParseException {
        Consumer createdConsumer = utils.createConsumer(consumer);
        scenarioContext.setContext(CONSUMER,  createdConsumer);
        loginPage.open().login(createdConsumer);
    }

    @Then("the consumer should see the error message {string}")
    public void theConsumerShouldSeeTheErrorMessage(String errorMessage) {
        String actualErrorMessage = loginPage.getErrorMessage();
        Assert.assertEquals(actualErrorMessage, errorMessage);
    }
}
