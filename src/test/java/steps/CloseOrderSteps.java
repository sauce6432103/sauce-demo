package steps;

import com.google.inject.Inject;
import commons.Consumer;
import commons.ScenarioContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pages.CartPage;
import pages.CheckoutPage;
import pages.InventoryPage;

import static commons.Context.*;

public class CloseOrderSteps {

    @Inject
    private InventoryPage inventoryPage;
    @Inject
    private CartPage cartPage;
    @Inject
    private CheckoutPage checkoutPage;
    @Inject
    private ScenarioContext scenarioContext;


    @When("the consumer places an order with {int} items")
    public void theConsumerPlacesAnOrderWithItems(Integer items) {
        Consumer selectedConsumer = scenarioContext.getContext(CONSUMER);
        Double subtotal = inventoryPage
            .addProductToCart(items)
            .goToCart()
            .getPrice(items);
        scenarioContext.setContext(SUBTOTAL, subtotal);
        Double total = cartPage
                .goToCheckout()
                .fillForm(selectedConsumer)
                .continueCheckout()
                .getPrice(items);
        scenarioContext.setContext(TOTAL, total);
        checkoutPage
                .continueCheckout();
    }

    @When("the consumer adds an item to cart")
    public void theConsumerAddsAnItemToCart() {
        Consumer selectedConsumer = scenarioContext.getContext(CONSUMER);
        inventoryPage
                .addProductToCart(1)
                .goToCart();

    }

    @And("the consumer removes the item from cart")
    public void theConsumerRemovesTheItemFromCart() {
        cartPage.removeItem(0);
    }

    @Then("the order should be successfully closed")
    public void theOrderShouldBeSuccessfullyClosed() throws InterruptedException {
        Double subtotal = scenarioContext.getContext(SUBTOTAL);
        Double total = scenarioContext.getContext(TOTAL);
        Assert.assertTrue(checkoutPage.isCheckoutComplete());
        Assert.assertEquals(total, subtotal);
    }

    @Then("the cart should be empty")
    public void theCartShouldBeEmpty() {
        Assert.assertEquals(cartPage.getItemsCount(), 0);
    }

    @Then("the order should not be successfully closed")
    public void theErrorMessageShouldBeDisplayed() throws InterruptedException {
        Assert.assertFalse(checkoutPage.isCheckoutComplete());
    }
}
