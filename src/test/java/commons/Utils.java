package commons;

import io.cucumber.guice.ScenarioScoped;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.io.FileReader;
import java.io.IOException;

import static java.lang.Thread.sleep;
import static steps.Setup.driver;

@ScenarioScoped
public class Utils {

    public JSONObject getJsonData(String fileName) throws IOException, ParseException {
        JSONParser jsonparser = new JSONParser();
        FileReader reader = new FileReader(fileName);
        Object obj = jsonparser.parse(reader);
        return (JSONObject)obj;
    }

    public Consumer createConsumer(String consumer) throws IOException, ParseException {
        JSONObject consumerJson = (JSONObject) new Utils().getJsonData("src/test/resources/support/data.json").get(consumer);
        return new Consumer(
                consumerJson.get("user").toString(),
                consumerJson.get("password").toString(),
                consumerJson.get("firstName").toString(),
                consumerJson.get("lastName").toString(),
                consumerJson.get("postalCode").toString());
    }

    public static boolean isDisplayed(By elementSelector) throws InterruptedException {
        boolean isDisplayed = false;
        for (int i = 0; i < 3; i++) {
            try {
                isDisplayed = driver.findElement(elementSelector).isDisplayed();
                break;
            } catch (NoSuchElementException e) {
                sleep(1000);
            }
        }
        return isDisplayed;
    }
}
