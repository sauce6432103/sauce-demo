package commons;

public class Consumer {
    private final String user;
    private final String password;
    public final String firstName;
    public final String lastName;
    public final String postalCode;

    public Consumer(String user, String password, String firstName, String lastName, String postalCode) {
        this.user = user;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPostalCode() {
        return postalCode;
    }
}
