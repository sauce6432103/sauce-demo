package commons;

import io.cucumber.guice.ScenarioScoped;

import java.util.HashMap;
import java.util.Map;

@ScenarioScoped
public class ScenarioContext {

    private final Map<Context, Object> contextMap;

    public ScenarioContext() {
        this.contextMap = new HashMap<>();
    }

    public <T> void setContext(Context key, T object) {
        contextMap.put(key, object);
    }

    @SuppressWarnings("unchecked")
    public <T> T getContext(Context key) {
        return (T) contextMap.get(key);
    }
}
