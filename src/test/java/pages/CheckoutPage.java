package pages;

import commons.Consumer;
import io.cucumber.guice.ScenarioScoped;
import org.openqa.selenium.By;

import java.util.ArrayList;

import static commons.Utils.isDisplayed;
import static steps.Setup.driver;

@ScenarioScoped
public class CheckoutPage {
    private final By inptFirstName = By.ByCssSelector.cssSelector("input[data-test='firstName']");
    private final By inptLastName = By.ByCssSelector.cssSelector("input[data-test='lastName']");
    private final By inptPostalCode = By.ByCssSelector.cssSelector("input[data-test='postalCode']");
    private final By btnContinue = By.ByCssSelector.cssSelector(".cart_button");

    private final By completeContainer = By.ByCssSelector.cssSelector("#checkout_complete_container");

    private final By totalPrice = By.ByCssSelector.cssSelector(".inventory_item_price");
    public CheckoutPage fillForm(Consumer consumer) {
        driver.findElement(inptFirstName).sendKeys(consumer.firstName);
        driver.findElement(inptLastName).sendKeys(consumer.lastName);
        driver.findElement(inptPostalCode).sendKeys(consumer.postalCode);
        return this;
    }

    public CheckoutPage continueCheckout() {
        driver.findElement(btnContinue).click();
        return this;
    }

    public boolean isCheckoutComplete() throws InterruptedException {
       return isDisplayed(completeContainer);
    }

    public double getPrice(int items) {
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < items; i++) {
            prices.add(Double.parseDouble(driver.findElements(totalPrice).get(i).getText().replace("$", "")));
        }
        return prices.stream().mapToDouble(Double::doubleValue).sum();
    }
}
