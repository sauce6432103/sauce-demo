package pages;

import io.cucumber.guice.ScenarioScoped;
import org.openqa.selenium.By;

import static steps.Setup.driver;

@ScenarioScoped
public class InventoryPage {

    private static final String URL = "https://www.saucedemo.com/inventory.html";
    private final By btnAddToCart = By.ByCssSelector.cssSelector(".inventory_item button");
    private final By btnCart = By.ByCssSelector.cssSelector("#shopping_cart_container");

    public InventoryPage addProductToCart(int quantity) {
        for (int i = 0; i < quantity; i++) {
            driver.findElements(btnAddToCart).get(i).click();
        }
        return this;
    }

    public CartPage goToCart() {
        driver.findElement(btnCart).click();
        return new CartPage();
    }
}
