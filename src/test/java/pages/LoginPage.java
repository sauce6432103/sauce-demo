package pages;

import commons.Consumer;
import io.cucumber.guice.ScenarioScoped;
import org.openqa.selenium.By;

import static steps.Setup.driver;

@ScenarioScoped
public class LoginPage {

    private static final String URL = "https://www.saucedemo.com/";

    private final By inptUsername = By.ByCssSelector.cssSelector("input[data-test='username']");
    private final By inptPassword = By.ByCssSelector.cssSelector("input[data-test='password']");
    private final By btnLogin = By.ByCssSelector.cssSelector("input[data-test='login-button']");
    private final By errorAlert = By.ByCssSelector.cssSelector("h3[data-test='error']");

    public LoginPage open() {
        driver.get(URL);
        return this;
    }

    public LoginPage login(Consumer consumer) {
        driver.findElement(inptUsername).sendKeys(consumer.getUser());
        driver.findElement(inptPassword).sendKeys(consumer.getPassword());
        driver.findElement(btnLogin).click();
        return this;
    }

    public String getErrorMessage() {
        return driver.findElement(errorAlert).getText();
    }
}
