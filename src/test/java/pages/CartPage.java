package pages;

import io.cucumber.guice.ScenarioScoped;
import org.openqa.selenium.By;

import java.util.ArrayList;
import static steps.Setup.driver;

@ScenarioScoped
public class CartPage {
    private final By items = By.ByCssSelector.cssSelector(".cart_item");
    private final By btnCheckout = By.ByCssSelector.cssSelector(".checkout_button");
    private final By btnRemove = By.ByCssSelector.cssSelector(".cart_item .cart_button");
    private final By price = By.ByCssSelector.cssSelector(".inventory_item_price");

    public int getItemsCount() {
        return driver.findElements(items).size();
    }

    public CheckoutPage goToCheckout() {
        driver.findElement(btnCheckout).click();
        return new CheckoutPage();
    }

    public CartPage removeItem(int index) {
        driver.findElements(btnRemove).get(index).click();
        return this;
    }

    public double getPrice(int items) {
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < items; i++) {
            prices.add(Double.parseDouble(driver.findElements(price).get(i).getText().replace("$", "")));
        }
        return prices.stream().mapToDouble(Double::doubleValue).sum();
    }
}
