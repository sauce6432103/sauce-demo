Pre requisitos:
- Java 11
- Maven 3.6.3

Para executar o projeto, basta executar o comando abaixo:
```shell script
maven clean test
```

Para gerar o relatório de testes, basta executar os comandos abaixo:
```shell script
npm install --save-dev allure-commandline
npx allure generate -c -o allure-report .\target\allure-results
npx allure open
```

1 - Testes são uma das ferramentas para elevar o nivel de qualidade, trata se de uma ação para validar que um ou mais critérios estão de acordo
com o planejado. Já qualidade refere se a todo o conjunto de caracteristicas de um software que podem ser avaliados e medidos
e que devem possuir altos niveis para garantir satisfação para usuarios.

2 - Deve ser criado um plano de testes baseado nos critérios de aceitação, com isso devem ser avaliados os cenários de maior impacto junto a equipe
desse modo iniciar os testes principais da implementação/feature/sistema que seria o smoke teste, após sucesso nos principais cenários, passariamos aos
cenários de menor impacto, o ideal é que os testes iniciem desde a fase de desenvolvimento, de forma que toda a equipe auxilie na validação para
encontrar bugs o quanto antes

3 - Na metodologia ágil, os testes são realizados continuamente ao longo do desenvolvimento, com ênfase em colaboração e entrega contínua, a cada novo componente/feature/fix deve ser realizado testes para garantir a qualidade de cada parte que está sendo desenvolvida.
Na waterfall, os testes são mais formais e ocorrem após a implementação completa, de modo que a validação fica quase que toda para o final, quando o componente/feature/fix está completo.

### Planejamento de Testes:

- 1 - Consumer se autentica e realiza uma compra
- 2 - Consumer se autentica e realiza uma compra com mais de um produto
- 3 - Consumer se autentica adiciona item ao carrinho e depois o remove
- 4 - Consumer se autentica e segue realizando uma compra sem adicionar nenhum produto ao carrinho
- 5 - Consumer se autentica e filtra produtos na InventoryPage
- 6 - Consumer se autentica avança ao carrinho com um produto e retorna no botão "Continue Shopping"
- 7 - Consumer se autentica avança ao checkout e retorna no botão "Cancel"
- 8 - Consumer se autentica e tenta avançar no checkout sem preencher formulario
- 9 - Consumer se autentica e tenta avançar no checkout preenchendo apenas firstName
- 10 - Consumer se autentica e tenta avançar no checkout preenchendo apenas lastName
- 11 - Consumer se autentica e tenta avançar no checkout preenchendo apenas zipcode
- 12 - Consumer se autentica e tenta avançar no checkout preenchendo zipcode inválido
- 13 - Consumer tenta se autenticar com email inválido
- 14 - Consumer tenta se autentica com senha inválida
- 15 - Consumer tenta se autenticar com usuário no status locked out

### Estratégia de testes:
- 1 - Eu tentaria reduzir o tempos dessas validações dividindo o teste entre membros da equipe (Devs e QAs) já que se trata de uma demanda de extrema urgência e possui um histórico de rollback
- 2 - Validaria primeiro os cenários core da nova feature (smoke) 16h
- 3 - Validaria os cenários regressivos (regression) para garantir que o que ja estava funcionando segue funcionando. 20h
- 4 - Se não fosse possível terminar os regressivos validaria os cenários de maior importância dentre eles, realizando a cobertura de ao menos 80% do sistema.
- 5 - Com o time todo validando e o tempo de validação sendo reduzido seria possivel realizar os testes exploratórios, neste caso eu os faria, do contrário apenas o smoke e o regressivo

### Testes automatizados:

- 1 - Fluxo de realizar uma compra com consumer autenticado
- 2 - Fluxo de logins inválidos
- 3 - Fluxos de remover itens do carrinho
- 4 - Fluxos de avançar ao checkout sem itens no carrinho
- 5 - Fluxos de não preencher o formulário e tentar seguir no checkout
- 6 - Fluxo de avançar ao checkout com mais de um item no carrinho

### Gestão de incidentes:
##### Bug de implementação encontrado:

- Descrição: Ao inserir um postal code inválido é exibida tela com a mensagem "Internal Server Error"
- Passos para reprodução:
    - 1 - Consumer se autentica
    - 2 - Consumer adiciona um produto ao carrinho e avança ao checkout
    - 3 - Consumer preenche firstName e lastName
    - 4 - Consumer insere um postal code inválido ex: 11111 e clica em continue

- Resultado Esperado: Deveria ser exibido um alerta de erro indicando que o postal code é inválido
- Resultado Obtido: Consumer é redirecionado a uma tela com a mensagem "Internal Server Error"
- Impacto: Consumer é apresentado a uma tela de erro diferente do layout do site o que pode causar insegurança e não deixa claro o motivo do erro que seria o de postal code incorreto.
- Criticidade: Medium

##### Bug de arquitetura encontrado:
- Descrição: Ao concluir o pedido a página demora cerca de 5 minutos para apresentar mensagem de sucesso
- Passos para reprodução:
    - 1 - Consumer se autentica
    - 2 - Consumer adiciona um produto ao carrinho e avança ao checkout
    - 3 - Consumer preenche formulário com dados válidos e avança finalizando o pedido

- Resultado Esperado: Consumer deveria ser redirecionado para o step de sucesso do checkout indicando que o pedido foi concluido com sucesso
- Resultado Obtido: Consumer demora cerca de 5 minutos para ser redirecionado a página de sucesso e obter feedback de compra concluida.
- Impacto: Isso Pode fazer com que o consumer abandone a página, atualize a página o que faria com que o pedido fosse perdido, isso também poderá sobrecarregar a equipe de suporte com reclamações
- Criticidade: Blocker


##### Bug de requisitos:

- Descrição: Na etapa de revisão do pedido o valor total não é exibido

- Passos para reprodução:
    - 1 - Consumer se autentica
    - 2 - Consumer adiciona um produto ao carrinho e avança ao checkout
    - 3 - Consumer preenche formulário com dados válidos e avança para a revisão de pedido

- Resultado Esperado: Deveria ser exibido o valor total do pedido na página de revisão
- Resultado Obtido: A informação do valor total do pedido não é exibida
- Impacto: Consumer não consegue visualizar valor do seu pedido antes de seguir para pagamento, gerando assim um má experiência.
- Criticidade: High













